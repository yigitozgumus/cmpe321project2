import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by yigitozgumus on 4/1/16.
 * This Class contains page header method
 */
 class Page  {

    //Data Fields for the page creation
    Integer pageId;



    public Page(String fileName,Integer pageId,String nextPagePointer,String isFull,Integer maxRecord) throws IOException {
        // Open a FileWriter object in the file
        int filler ;
        FileWriter store  = new FileWriter(fileName,true);
        store.write( pageId.toString());
        filler = 4 - pageId.toString().length();
        for (int i = 0; i < filler; i++) {store.write(" ");}
        store.write("|");
        store.write(nextPagePointer);
        store.write("|");
        store.write(isFull);
        store.write("|");
        store.write(maxRecord.toString());
        filler = 4 - maxRecord.toString().length();
        for (int i = 0; i < filler; i++) {store.write(" ");}
        store.write("|");
        store.write("\n");
        store.close();
    }
}
