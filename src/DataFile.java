import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by yigitozgumus on 4/2/16.
 * This class will define the DataFiles of the types
 */
class DataFile {
    //Data Fields
    String typeName ;
    String filePrefix = "Database/" ;
    String nextPagePointer = "PAGENULL";
    String isFull = "0";

    Integer pageId = 0;
    Integer maxRecordPerPage = 25;
    Integer currentRecords = 0;
    int pageHeader = 22;
    int recordSize = 79;
    File dataFile ;
    boolean initialPage = true;
    boolean initialRecord = true;
    ArrayList<Page> pages ;

    public DataFile(String fileName) throws IOException {
        pages = new ArrayList<Page>();
        String typeFileName = filePrefix + fileName + ".dat";
        typeName = fileName;
        dataFile = new File(typeFileName);
        dataFile.createNewFile();
        pages.add(new Page(dataFile.getAbsolutePath(),pageId,nextPagePointer,isFull,0));
        pageId++;
    }

    public void expandSize() throws IOException {
        //Get the last page in the Syscat
        //TODO add check method
        RandomAccessFile fileUpdater = new RandomAccessFile(dataFile.getAbsolutePath(),"rw");
        if (!initialPage){
            fileUpdater.seek(5);
            fileUpdater.writeBytes("PAGE1   ");
            pages.add(new Page(dataFile.getAbsolutePath(),pageId,nextPagePointer,isFull,0));
            pageId++;
            initialPage = false;
            fileUpdater.close();
        }else{
            fileUpdater = new RandomAccessFile(dataFile.getAbsolutePath(),"rw");
            fileUpdater.seek(14 + (pageId * pageHeader + maxRecordPerPage * recordSize));
            fileUpdater.writeBytes("1");
            fileUpdater.seek(5 + ((pageId-1) * ((maxRecordPerPage * recordSize) + pageHeader)));
            Integer pointer = ((pageId) * ((maxRecordPerPage * recordSize) + pageHeader));
            String point = String.valueOf(pointer);
            int filler = 8 - point.length();
            for (int i = 0; i < filler; i++) {point+=" ";}
            fileUpdater.writeBytes(point);
            pages.add(new Page(dataFile.getAbsolutePath(),pageId,nextPagePointer,isFull,0));
            pageId++;

        }
    }
    public void addRecord(String instanceName,String nextRecordPointer,String isDeleted,ArrayList<String> fieldValues) throws IOException{
        int filler ;
        //TODO Check if page is full then do this if full check deleted ones
        FileWriter recorder = new FileWriter(dataFile.getAbsolutePath(),true);
        //Write the record type
        recorder.write(instanceName);
        filler = 16 - instanceName.length();
        for (int i = 0; i < filler; i++) {recorder.write(" ");}
        recorder.write("|");
        // Write the next record pointer
        if(initialRecord){
            recorder.write(nextRecordPointer);
            initialRecord = false;
        }else{
            //Previous record pointer update
            RandomAccessFile ptrUpdate = new RandomAccessFile(dataFile.getAbsolutePath(),"rw");
            if(currentRecords % maxRecordPerPage ==0 && currentRecords>=maxRecordPerPage){
                ptrUpdate.seek((pageHeader * (pageId-1)) +(17 + (currentRecords -1) * recordSize));
            }else {
                ptrUpdate.seek((pageHeader * pageId) + (17 + (currentRecords - 1) * recordSize));
            }
            Integer pointData = ((pageId) * pageHeader) + ( (currentRecords)* recordSize );
            String point = String.valueOf(pointData);
            filler = 8 - point.length();
            for (int i = 0; i < filler; i++) {point = point + " ";}
            ptrUpdate.writeBytes(point);
            recorder.write(nextRecordPointer);
            ptrUpdate.close();
        }
        recorder.write("|");
        // Write the isDeleted
        recorder.write(isDeleted);
        recorder.write("|");
        // Write the Field Values
        for (String fieldValue : fieldValues) {
            recorder.write(fieldValue);
            filler = 4 - fieldValue.length();
            for (int i = 0; i < filler; i++) {
                recorder.write(" ");
            }
            recorder.write("|");
        }
        for (int index = 0; index <(10 - fieldValues.size()) ; index++) {
            recorder.write("    ");
            recorder.write("|");
        }
        recorder.write("\n");
        recorder.close();
        // End of the record object
        ++currentRecords;
        //==============================================================================================================
        //Update Page if full
        if(currentRecords%maxRecordPerPage ==0 && currentRecords>=maxRecordPerPage){
            RandomAccessFile ptrUpdate = new RandomAccessFile(dataFile.getAbsolutePath(),"rw");
            if (initialPage){
                ptrUpdate.seek(14);
                ptrUpdate.writeBytes("1");
            }else{
                ptrUpdate.seek(14 + ((pageId-1) * (this.pageHeader + maxRecordPerPage * recordSize)));
                ptrUpdate.writeBytes("1");
            }
            ptrUpdate.close();
        }
        //==============================================================================================================
        // Update the current number of records
        //==============================================================================================================
        RandomAccessFile rcrUpdate = new RandomAccessFile(dataFile.getAbsolutePath(),"rw");
        if(initialPage) {
            rcrUpdate.seek(16);
        }else{
            rcrUpdate.seek(16 + (pageId-1) * (pageHeader + recordSize * maxRecordPerPage));
        }
        Integer index = currentRecords% maxRecordPerPage != 0 ? currentRecords%maxRecordPerPage : maxRecordPerPage;
        String update = index.toString();
        filler = 4 - update.length();
        for (int i = 0; i < filler; i++) {update.concat(" ");}
        rcrUpdate.writeBytes(update);
        rcrUpdate.close();

    }

    public ArrayList<ArrayList<String>> getHeaders() throws IOException {
        RandomAccessFile instanceRead = new RandomAccessFile(dataFile.getAbsolutePath(),"r");
        ArrayList<ArrayList<String>> headerList = new ArrayList<ArrayList<String>>();
        ArrayList<String> header = new ArrayList<String>();
        String line ;
        //Read the firs Header
        line = instanceRead.readLine();
        String[] pageH = line.split("\\|");
        Collections.addAll(header, pageH);

        if(header.get(1).equals("PAGENULL")) {
            headerList.add(header);
            return headerList;
        }else{
            headerList.add(header);
            for (int i = 1; i <= (pageId-1) ; i++) {
                ArrayList<String> fillR = new ArrayList<String>();
                instanceRead.seek((i) * (this.pageHeader + (maxRecordPerPage * recordSize)));
                line = instanceRead.readLine();
                String[] pageH2 = line.split("\\|");
                Collections.addAll(fillR, pageH2);
                headerList.add(fillR);
            }
            return headerList;
        }
    }

    public ArrayList<ArrayList<String>> getDataRecords() throws IOException {
        RandomAccessFile recordReader = new RandomAccessFile(dataFile.getAbsolutePath(),"r");
        ArrayList<ArrayList<String>> records = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> pageHeader = getHeaders();
        String line ;
        for (int i = 0; i <pageHeader.size() ; i++) {
            recordReader.readLine();
            int recSize = Integer.parseInt(pageHeader.get(i).get(3).replaceAll("\\s",""));
            int j = 0;
            while (j < recSize) {
                ArrayList<String> record = new ArrayList<String>();
                line = recordReader.readLine();
                String[] elements = line.split("\\|");
                Collections.addAll(record, elements);
                records.add(record);
                j++;
            }
        }
        return records;
    }

    public ArrayList<String> getRecord(int keyField) throws IOException {
        // first pointer ->22 , +17 pointer field, + 28 primary field
        ArrayList<String> result = new ArrayList<String>();
        RandomAccessFile finder = new RandomAccessFile(dataFile.getAbsolutePath(),"r");
        Integer wantedKey = 999999;
        int location = pageHeader;
        finder.seek(location);
        while(wantedKey != keyField){
            finder.seek(location + 28);
            byte[] keyInput = new byte[4];
            finder.readFully(keyInput);
            String transitionKey ="";
            for (int i = 0; i <4 ; i++) {transitionKey+=Character.toString((char) keyInput[i]);}
            wantedKey = Integer.parseInt(transitionKey.replaceAll("\\s",""));
            if (wantedKey != keyField){
                finder.seek(location + 17);
                byte[] pointInput = new byte[8];
                finder.readFully(pointInput);
                String transitionPoint="";
                for (int i = 0; i <8 ; i++) {transitionPoint+=Character.toString((char) pointInput[i]);}
                location = Integer.parseInt(transitionPoint.replaceAll("\\s",""));
            }
        }
        finder.seek(location);
        String output = finder.readLine();
        String[] elements = output.split("\\|");
        Collections.addAll(result,elements);
        return result ;
    }

    public void deleteOneRecord(int keyField) throws IOException{
        // first pointer ->22 , +17 pointer field, + 28 primary field
        ArrayList<String> result = new ArrayList<String>();
        RandomAccessFile finder = new RandomAccessFile(dataFile.getAbsolutePath(),"rw");
        Integer wantedKey = 999999;
        int location = pageHeader;
        finder.seek(location);
        while(wantedKey != keyField){
            byte[] keyInput = new byte[4];
            finder.seek(location + 28);
            finder.readFully(keyInput);
            String transitionKey ="";
            for (int i = 0; i <4 ; i++) {transitionKey+=Character.toString((char) keyInput[i]);}
            wantedKey = Integer.parseInt(transitionKey.replaceAll("\\s",""));
            if (wantedKey != keyField){
                byte[] pointInput = new byte[8];
                finder.seek(location + 17);
                finder.readFully(pointInput);
                String transitionPoint="";
                for (int i = 0; i <8 ; i++) {transitionPoint+=Character.toString((char) pointInput[i]);}
                location = Integer.parseInt(transitionPoint.replaceAll("\\s",""));
            }
        }
        finder.seek(location +26);
        finder.writeBytes("1");
        finder.close();
    }

}
