import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {

        DBMS database = new DBMS();
        int button = 666;
        Scanner scanner = new Scanner(System.in);
        while(button != 0){
            System.out.println("Here is the list of Operations, choose a number to execute:");
            System.out.println("1. Create a type");
            System.out.println("2. Delete a type");
            System.out.println("3. List all types");
            System.out.println("4. Create a record");
            System.out.println("5. Delete a record");
            System.out.println("6. Search a record with primary key");
            System.out.println("7. List all instances of a type");
            System.out.println("0. Exit the program");
            if (scanner.hasNextInt()) {
                button = scanner.nextInt();
            }else{
                button = 666 ;
            }
            switch (button){
                case 1: database.createType(); break;
                case 2: database.deleteType(); break;
                case 3: database.listAllTypes(); break;
                case 4: database.createRecord(); break;
                case 5: database.deleteRecord(); break;
                case 6: database.searchRecord(); break;
                case 7: database.listRecordsOfType(); break;
                case 0: break;
                default:
                    System.out.println("Invalid command, try again"); break;
            }
        }

    }
}
