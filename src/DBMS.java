/**
 * Created by yigitozgumus on 4/1/16.
 * This class contains the DBMS Object
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

class DBMS {
    //Data Fields
    String filePrefix = "Database/";
    int maxFieldNumber = 10;
    SystemCatalogue sysCat ;
    ArrayList<DataFile> dataFiles ;

    public DBMS() throws IOException {
        initialize();
    }

    public void initialize() throws IOException {
        //TODO add a check method for syscat
        String databaseFolder = "Database";
        File restart= new File(databaseFolder);
        if(restart.exists() && restart.isDirectory()){
            restart.delete();
        }
        boolean success = (new File(databaseFolder).mkdirs());
        sysCat = new SystemCatalogue();
        dataFiles = new ArrayList<DataFile>();

    }

    public boolean createType() throws IOException {
        Scanner getStr = new Scanner(System.in);
        System.out.println("Please enter the type name (max 16 characters):");
        String typeName = getStr.next();
        System.out.println("Please enter the primary key field and then the others(press q to exit)");
        System.out.println("max 15 characters, maximum Field number is 10:");
        ArrayList<String> getFields = new ArrayList<String>();
        for (int index = 0; index < maxFieldNumber; index++) {
            String input = getStr.next();
            if(input.equals("q")){
                break;
            }else {
                getFields.add(input);
            }
        }
        if(sysCat.currentRecords% sysCat.maxRecordSize == 0 && sysCat.currentRecords>=sysCat.maxRecordSize){
            sysCat.updateSize();
        }

        sysCat.addRecord(typeName,"RCRDNULL",sysCat.firstPageId,"0",getFields);
        dataFiles.add(new DataFile(typeName));
        return true;
    }
    public void listAllTypes() throws IOException {
        System.out.println("|Type name-------|Field Names ---|---------------|---------------|---------------|-----------" +
                "----|---------------|---------------|---------------|---------------|---------------|");
        ArrayList<ArrayList<String>> records = sysCat.getRecords();
        for(ArrayList<String> data : records){
            if(data.get(3).equals("0")){
                System.out.print("|"+data.get(0)+"|");
                for (int i = 4; i < data.size(); i++) {
                    System.out.print(data.get(i) +"|");
                }
                System.out.println();
            }
        }
        System.out.println("|----------------|---------------|---------------|---------------|---------------|-----------" +
                "----|---------------|---------------|---------------|---------------|---------------|");
    }
    public void deleteType() throws IOException {
        Scanner getStr = new Scanner(System.in);
        System.out.println("Please enter the type name you want to delete (max 16 characters):");
        String typeName = getStr.next();
        ArrayList<ArrayList<String>> records = sysCat.getRecords();
        boolean change = false ;
        int index = 0;
        for (int i=0;i<records.size();i++){
            if(records.get(i).get(0).replaceAll("\\s","").equals(typeName)){
                index = i;
                change = true ;
            }
        }
        if (change) {
            sysCat.deleteOneRecord(index);
        }else{
            System.out.println("Either you entered wrong type name or the type does not exist.");
        }

    }
    public void createRecord() throws IOException {
        //TODO implement check from the syscat
        Scanner getStr = new Scanner(System.in);
        System.out.println("Please enter the type name (max 16 characters):");
        String typeName = getStr.next();
        ArrayList<ArrayList<String>> records = sysCat.getRecords();
        ArrayList<String> wanted = new ArrayList<String>();
        boolean check1 = false;
        for(ArrayList<String> data: records){
            if(data.get(0).replaceAll("\\s","").equals(typeName)){
                check1 = true;
                wanted = (ArrayList<String>) data.clone();
            }
        }
        int fieldQuantity = 0;
        for (int i = 4; i < wanted.size();i++){
            if (!wanted.get(i).startsWith(" ")){
                fieldQuantity++;
            }
        }
        System.out.println("Please enter the Instance name (max 16 characters):");
        String instanceName = getStr.next();
        System.out.println("Please enter the field values (total of "+fieldQuantity+" values) (press q to exit)");
        System.out.println("max 4 digits.");
        ArrayList<String> getFields = new ArrayList<String>();
        for (int index = 0; index < fieldQuantity; index++) {
            System.out.print(index==0?(wanted.get(index +4).replaceAll("\\s","")+" (primary key): "):wanted.get(index +4).replaceAll("\\s","")+": ");
            String input = getStr.next();
            if(input.equals("q")){
                break;
            }else {
                getFields.add(input);
            }
        }

        if (check1) {
            for (DataFile df : dataFiles) {
                if (df.typeName.equals(typeName)) {
                    if(df.currentRecords% df.maxRecordPerPage == 0 && df.currentRecords>=df.maxRecordPerPage){
                        df.expandSize();
                    }
                    df.addRecord(instanceName, "RCRDNULL", "0", getFields);
                }
            }
        }
    }
    public void deleteRecord() throws IOException {
        Scanner getStr = new Scanner(System.in);
        System.out.println("Please enter the type name you want to delete (max 16 characters):");
        String typeName = getStr.next();
        System.out.println("Please enter the primary key field of the record you want to delete:");
        System.out.println("max 4 digits");
        String keyField = getStr.next();
        boolean validation = false;
        int dataIndex =0;
        for (int i = 0;i<dataFiles.size();i++){
            if(dataFiles.get(i).typeName.replaceAll("\\s","").equals(typeName)){
                validation = true;
                dataIndex = i;

            }
        }
        if (validation){
            dataFiles.get(dataIndex).deleteOneRecord(Integer.parseInt(keyField));
            System.out.println("The record is deleted");
        }else{
            System.out.println();
        }

    }
    public void searchRecord() throws IOException {
        //TODO implement a validation
        Scanner getInput = new Scanner(System.in);
        ArrayList<String> output = new ArrayList<String>();
        System.out.println("Please enter the type name (max 16 characters):");
        String typeName = getInput.next();
        System.out.println("Please enter the primary key field value to search: ");
        System.out.println("max 4 digits");
        String primaryKey = getInput.next();
        for (DataFile data:dataFiles){
            if(data.typeName.replaceAll("\\s","").equals(typeName)){
                output = data.getRecord(Integer.parseInt(primaryKey));
            }
        }
        if (output.get(2).equals("0")) {
            System.out.println("|Instance Name---|Primary Field--|Field Values --|---------------|---------------|-----------" +
                    "----|---------------|---------------|---------------|---------------|---------------|");
            System.out.print("|" + output.get(0) + "|");
            for (int i = 3; i < output.size(); i++) {
                System.out.print(output.get(i).concat("           ") + "|");
            }
            System.out.println();
            System.out.println("|----------------|---------------|---------------|---------------|---------------|-----------" +
                    "----|---------------|---------------|---------------|---------------|---------------|");
        }else{
            System.out.println("The record you search is deleted from the Database.");
        }

    }
    public void listRecordsOfType() throws IOException {
        boolean check = false ;
        ArrayList<String> type = new ArrayList<String>();
        Scanner getStr = new Scanner(System.in);
        System.out.println("Please enter the type name (max 16 characters):");
        String typeName = getStr.next();
        System.out.println();
        ArrayList<ArrayList<String>> sysRecords = sysCat.getRecords();
        ArrayList<ArrayList<String>> dataRecords = new ArrayList<ArrayList<String>>();
        for (ArrayList<String> data:sysRecords){
            if (data.get(0).replaceAll("\\s","").equals(typeName)){
                check = true ;
                type = (ArrayList<String>) data.clone();
            }
        }
        // TODO crash on wrong input

        if(type.get(3).equals("1")){
            System.out.println("This type is deleted from the system");
            return;
        }
        if(check){
            System.out.println("|Type name-------|Field Names ---|---------------|---------------|---------------|-----------" +
                    "----|---------------|---------------|---------------|---------------|---------------|");
            System.out.print("|" +type.get(0)+"|");
            for (int i = 4; i < type.size(); i++) {
                System.out.print(type.get(i) +"|");
            }
            System.out.println();
            System.out.println("|Instance Name---|Primary Field--|Field Values --|---------------|---------------|-----------" +
                    "----|---------------|---------------|---------------|---------------|---------------|");
            for (DataFile data:dataFiles){
                if (data.typeName.equals(typeName)){
                    dataRecords = data.getDataRecords();
                }
            }
            for(ArrayList<String> data : dataRecords){
                if (data.get(2).equals("0")) {
                    System.out.print("|" + data.get(0) + "|");
                    for (int i = 3; i < data.size(); i++) {
                        System.out.print(data.get(i).concat("           ") + "|");
                    }
                    System.out.println();
                }
            }
            System.out.println("|----------------|---------------|---------------|---------------|---------------|-----------" +
                    "----|---------------|---------------|---------------|---------------|---------------|");
        }else{
            System.out.println("Either you entered wrong type name or the type does not exist.");
        }


    }

}
