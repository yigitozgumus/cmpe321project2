import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by yigitozgumus on 4/1/16.
 * This class contains the System Catalogue object
 */

public class SystemCatalogue {

    //Data Fields of the System Catalogue
    Integer maxRecordSize = 10;
    Integer pageId = 0;
    Integer firstPageId = 0;
    Integer currentRecords = 0;
    int pageSize = 2048;
    int recordSize = 194 ;
    int pageHeader = 22 ;

    String filePrefix = "Database/";
    String nextPagePointer = "PAGENULL";
    String isFull = "0";
    boolean initialPage = true;
    boolean initialRecord = true;
    ArrayList<Page> pages ;
    File systemCatalogue ;


    public SystemCatalogue() throws IOException {

        systemCatalogue = new File( filePrefix + "Sys.cat");
        systemCatalogue.createNewFile();
        pages = new ArrayList<Page>();
        //Intial Page Header
        pages.add(new Page(systemCatalogue.getAbsolutePath(),pageId,nextPagePointer,isFull,0));
        pageId++;
    }

    public void updateSize() throws IOException {
        //Get the last page in the Syscat
        //TODO add check method
        RandomAccessFile fileUpdater = new RandomAccessFile(systemCatalogue.getAbsolutePath(),"rw");
        if (initialPage){
            fileUpdater.seek(5);
            fileUpdater.writeBytes("1962    ");
            initialPage = false;
            pages.add(new Page(systemCatalogue.getAbsolutePath(),pageId,nextPagePointer,isFull,0));
            pageId++;
            fileUpdater.close();
        }else{
            fileUpdater = new RandomAccessFile(systemCatalogue.getAbsolutePath(),"rw");
            fileUpdater.seek(5 + ((pageId-1) * ((maxRecordSize * recordSize) + pageHeader)));
            Integer pointer = ((pageId) * ((maxRecordSize * recordSize) + pageHeader));
            String point = String.valueOf(  pointer);
            int filler = 8 - point.length();
            for (int i = 0; i < filler; i++) {point+=" ";}
            fileUpdater.writeBytes(point);
            fileUpdater.seek(14 + (pageId * pageHeader + maxRecordSize * recordSize));
            fileUpdater.writeBytes("1");
            pages.add(new Page(systemCatalogue.getAbsolutePath(),pageId,nextPagePointer,isFull,0));
            pageId++;

        }
    }

    public void addRecord(String recordType,String nextRecordPointer,Integer firstPageId,String isDeleted,ArrayList<String> fieldNames) throws IOException{
        int filler ;

        //TODO Check if page is full then do this if full check deleted ones
        FileWriter recorder = new FileWriter(systemCatalogue.getAbsolutePath(),true);
        //Write the record type
        recorder.write(recordType);
        filler = 16 - recordType.length();
        for (int i = 0; i < filler; i++) {recorder.write(" ");}
        recorder.write("|");
        // Write the next record pointer
        if(initialRecord){
            recorder.write(nextRecordPointer);
            initialRecord = false;
        }else{//Previous record pointer update
            RandomAccessFile ptrUpdate = new RandomAccessFile(systemCatalogue.getAbsolutePath(),"rw");
            if(currentRecords % maxRecordSize ==0 && currentRecords>=maxRecordSize){
                ptrUpdate.seek((pageHeader * (pageId-1)) +(17 + (currentRecords -1) * recordSize));
            }else {
                ptrUpdate.seek((pageHeader * pageId) + (17 + (currentRecords - 1) * recordSize));
            }
            //String pointR = "RCRD" + currentRecords.toString();
            Integer pointR = ((pageId) * pageHeader) + ( (currentRecords)* recordSize );
            String point = String.valueOf(pointR);
            filler = 8 - point.length();
            for (int i = 0; i < filler; i++) {point+=" ";}
            ptrUpdate.writeBytes(point);
            recorder.write(nextRecordPointer);
            ptrUpdate.close();
        }
        recorder.write("|");
        // Write the first page Id
        recorder.write(firstPageId.toString());
        filler = 4 - firstPageId.toString().length();
        for (int i = 0; i < filler; i++) {recorder.write(" ");}
        recorder.write("|");
        // Write the isDeleted
        recorder.write(isDeleted);
        recorder.write("|");
        // Write the Field Names
        for (String fieldName : fieldNames) {
            recorder.write(fieldName);
            filler = 15 - fieldName.length();
            for (int i = 0; i < filler; i++) {
                recorder.write(" ");
            }
            recorder.write("|");
        }
        for (int index = 0; index <(10 - fieldNames.size()) ; index++) {
            recorder.write("               ");
            recorder.write("|");
        }
        recorder.write("\n");
        recorder.close();
        // End of the record object
        ++currentRecords;
        // Update the current number of records
        //==============================================================================================================
        RandomAccessFile rcrUpdate = new RandomAccessFile(systemCatalogue.getAbsolutePath(),"rw");
        if(initialPage) {
            rcrUpdate.seek(16);
        }else{
            rcrUpdate.seek(16 + (pageId-1) * (pageHeader + recordSize * maxRecordSize));
        }
        Integer index = currentRecords% maxRecordSize != 0 ? currentRecords%maxRecordSize : maxRecordSize;
        String update = index.toString();
        filler = 4 - update.length();
        for (int i = 0; i < filler; i++) {update.concat(" ");}
        rcrUpdate.writeBytes(update);
        rcrUpdate.close();
        //==============================================================================================================
        //Update Page if full
        if(currentRecords%maxRecordSize ==0 && currentRecords>=maxRecordSize){
            RandomAccessFile ptrUpdate = new RandomAccessFile(systemCatalogue.getAbsolutePath(),"rw");
            if (initialPage){
                ptrUpdate.seek(14);
                ptrUpdate.writeBytes("1");
            }else{
                ptrUpdate.seek(14 + ((pageId-1) * (pageHeader + maxRecordSize * recordSize)));
                ptrUpdate.writeBytes("1");
            }
            ptrUpdate.close();
        }
        //==============================================================================================================
    }

    public ArrayList<ArrayList<String>> getPageHeaders() throws IOException {
        RandomAccessFile typeRead = new RandomAccessFile(systemCatalogue.getAbsolutePath(),"r");
        ArrayList<ArrayList<String>> headers = new ArrayList<ArrayList<String>>();
        ArrayList<String> header = new ArrayList<String>();
        String line ;
        //Read the firs Header
        line = typeRead.readLine();
        String[] pageH = line.split("\\|");
        Collections.addAll(header, pageH);

        if(header.get(1).equals("PAGENULL")) {
            headers.add(header);
            return headers;
        }else{
            headers.add(header);
            for (int i = 1; i <= (pageId-1) ; i++) {
                ArrayList<String> fillR = new ArrayList<String>();
                typeRead.seek((i) * (pageHeader + (maxRecordSize * recordSize)));
                line = typeRead.readLine();
                String[] pageH2 = line.split("\\|");
                Collections.addAll(fillR, pageH2);
                headers.add(fillR);
            }
            return headers;
        }
    }

    public ArrayList<ArrayList<String>> getRecords() throws IOException {
        RandomAccessFile recordReader = new RandomAccessFile(systemCatalogue.getAbsolutePath(),"r");
        ArrayList<ArrayList<String>> records = new ArrayList<ArrayList<String>>();
        ArrayList<ArrayList<String>> pageHeader = getPageHeaders();
        String line ;
        for (int i = 0; i <pageHeader.size() ; i++) {
            recordReader.readLine();
            int recSize = Integer.parseInt(pageHeader.get(i).get(3).replaceAll("\\s",""));
            for (int j = 0; j < recSize ; j++) {
                ArrayList<String> record = new ArrayList<String>();
                line = recordReader.readLine();
                String[] elements = line.split("\\|");
                Collections.addAll(record, elements);
                records.add(record);
            }
        }
        return records;
    }
    public void deleteOneRecord(int index) throws IOException{
        RandomAccessFile deleter = new RandomAccessFile(systemCatalogue.getAbsolutePath(),"rw");
        deleter.seek((pageHeader *pageId) + ((index* recordSize) + 31));
        deleter.writeBytes("1");
    }


}
